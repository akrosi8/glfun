# OpenGL Experiments
This is the repository I use to store my creations as I learn OpenGL.
## Files
- pyr1.c
    - Draws a triangle. Will eventually be a pyramid.
- LoadShaders.c
    - A pure-C program to read, compile and load GLSL shaders from file.
