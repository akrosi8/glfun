#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

GLuint LoadShaders(){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Shader file pointers
    FILE* VertexShaderStream = fopen("./VertexShader.glsl", "r");
    FILE* FragmentShaderStream = fopen("./FragmentShader.glsl", "r");

    long int vffs,fffs;

    GLint Result = GL_FALSE;
    int InfoLogLength;
    // Vertex shader handling
    fseek(VertexShaderStream, 0L, SEEK_END);
    vffs = ftell(VertexShaderStream);
    rewind(VertexShaderStream);

    char VertexShaderData[vffs];

    fread(&VertexShaderData, 1, vffs, VertexShaderStream);
    VertexShaderData[vffs] = 0;
    const char *VertexShaderPointer = VertexShaderData;

    glShaderSource(VertexShaderID, 1, &VertexShaderPointer, NULL);
    printf("Vertex Shader code: \n%s\n", VertexShaderData);
    // Compile vertex shader
    glCompileShader(VertexShaderID);
    // Check for and report errors
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if(InfoLogLength != 0) {
        char VertexShaderErrorMessage[InfoLogLength+1];
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("Vertex shader error: %s\n", &VertexShaderErrorMessage[0]);
    }
    else
        printf("Vertex shader compiled successfully!\n");

    // Fragment shader handling
    fseek(FragmentShaderStream, 0L, SEEK_END);
    fffs = ftell(FragmentShaderStream);
    rewind(FragmentShaderStream);

    char FragmentShaderData[fffs];

    fread(&FragmentShaderData, 1, fffs, FragmentShaderStream);
    FragmentShaderData[fffs] = 0;
    const char *FragmentShaderPointer = FragmentShaderData;

    glShaderSource(FragmentShaderID, 1, &FragmentShaderPointer, NULL);
    printf("Fragment Shader code: \n%s\n", FragmentShaderData);

    // Compile fragment shader
    glCompileShader(FragmentShaderID);
    // Error checking and reporting
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if(InfoLogLength != 0) {
        char FragmentShaderErrorMessage[InfoLogLength+1];
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("Fragment shader error: %s\n", &FragmentShaderErrorMessage[0]);
    }
    else
        printf("Fragment shader compiled successfully!\n");

    // Program finalization
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);
    return ProgramID;
}
